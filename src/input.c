#include <stdio.h>
#include <allegro5/allegro5.h>
#include "structs.h"

int process_input(ALLEGRO_EVENT ev, struct game_state *state) {
  switch(ev.keyboard.keycode) {
  case ALLEGRO_KEY_A:
    state->input ^= 0b1; 
    break;
  case ALLEGRO_KEY_S:
    state->input ^= 0b10; 
    break;
  case ALLEGRO_KEY_D:
    state->input ^= 0b100;
    break;
  case ALLEGRO_KEY_W:
    state->input ^= 0b1000;
    break;
  case ALLEGRO_KEY_J:
    state->input ^=1 << 5;
    break;
  case ALLEGRO_KEY_K:
    state->input ^=1 << 6;
    break;
  case ALLEGRO_KEY_L:
    state->input ^=1 << 7;
    break;
  case ALLEGRO_KEY_I:
    state->input ^=1 << 8;
    break;
  }
  printf("%d\n",state->input);
  return 0;
}

int update_game(struct game_state *state) {
  if (state->input & 0b1) {
    state->rect1.x-=3;
  }
  if (state->input & 0b10) {
    state->rect1.y+=3;
  }
  if (state->input & 0b100) {
    state->rect1.x+=3;
  }
  if (state->input & 0b1000) {
    state->rect1.y-=3;
  }
  if (state->input & 1 << 5) {
    state->rect1.w-=3;
  }
  if (state->input & 1 << 6) {
    state->rect1.h+=3;
  }
  if (state->input & 1 << 7) {
    state->rect1.w+=3;
  }
  if (state->input & 1 << 8) {
    state->rect1.h-=3;
  }
  return 0;
}
