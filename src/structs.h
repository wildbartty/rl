#ifndef GAME_STRUCTS
#define GAME_STRUCTS
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>

struct rectangle {
  int h,w;
  int x,y; /* position on screen */
  ALLEGRO_COLOR color;
  //int dx,dy; /* current velocity */
};

struct ring_buffer {
  void *data;
  int length;
  int size; /* The size of an individual piece of data */
  int pos;
};

struct game_state {
  struct rectangle rect1;
  struct rectangle rect2;
  int input;
  int controls_config[256];
  ALLEGRO_FONT* font;
};

#endif
