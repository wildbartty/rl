#ifndef GAME_INPUT
#define GAME_INPUT
#include <allegro5/allegro.h>
#include "structs.h"

int process_input(ALLEGRO_EVENT, struct game_state*);
int update_game(struct game_state*);

#endif
