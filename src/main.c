#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "structs.h"
#include "input.h"
#include "render.h"

int main()
{
  al_init();
  al_init_primitives_addon();
  al_install_keyboard();

# ifndef S_SPLINT_S

  ALLEGRO_TIMER * timer = al_create_timer(1.0 / 60.0);
  ALLEGRO_EVENT_QUEUE * queue = al_create_event_queue();

  ALLEGRO_DISPLAY* disp = al_create_display(640, 420);
  #endif

  al_register_event_source(queue, al_get_keyboard_event_source());
  al_register_event_source(queue, al_get_display_event_source(disp));
  al_register_event_source(queue, al_get_timer_event_source(timer));

  
  struct game_state *game_state = malloc(sizeof(struct game_state));
  game_state->rect1.color = al_map_rgb(255, 0, 0);
  game_state->rect1.x = 100;
  game_state->rect1.y = 100;
  game_state->rect1.w = 100;
  game_state->rect1.h = 100;
  game_state->rect2.color = al_map_rgb(255, 0, 0);
  game_state->rect2.x = 100;
  game_state->rect2.y = 100;
  game_state->rect2.w = 100;
  game_state->rect2.h = 100;
  game_state->font = al_create_builtin_font();
  game_state->input = 0;
  ALLEGRO_EVENT event;

  al_start_timer(timer);
  while(1)
    {
      al_clear_to_color(al_map_rgb(0, 0, 0));
      al_wait_for_event(queue, &event);
      double last_time = al_get_time();
      switch(event.type) {
      case ALLEGRO_EVENT_KEY_DOWN:
      case ALLEGRO_EVENT_KEY_UP:
        process_input(event, game_state);
        break;
      case ALLEGRO_EVENT_TIMER:
        update_game(game_state);
        draw_game(game_state);
        draw_fps(game_state, last_time);
        al_flip_display();
        break;
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        goto break_loop;
      }
    }
 break_loop:

  al_destroy_font(game_state->font);
  al_destroy_display(disp);
  al_destroy_timer(timer);
  al_destroy_event_queue(queue);

  free(game_state);
  
  return 0;
}
