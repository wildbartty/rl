#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include "render.h"

int draw_rectangle(struct rectangle* rect)
{
  al_draw_filled_rectangle(rect->x + rect->w, rect->y + rect->h,rect->x, rect->y, rect->color);
  return 0;
}

int draw_game(struct game_state* state)
{
  struct rectangle rect = state->rect1;
  struct rectangle rect2 = state->rect2;

  al_draw_filled_rectangle(0, 0, 10, 100, al_map_rgb(0,255,0));
  draw_rectangle(&rect2);
  draw_rectangle(&rect);
    
  return 0;
}

int draw_fps(struct game_state* state, double last_frame) {
  ALLEGRO_FONT* font = state->font;
  char str[30];
  sprintf(str,"%lf", (al_get_time() - last_frame) * 100);
  al_draw_text(font, al_map_rgb(255, 255, 255), 20, 20, 0, str);
  return 0;
}
