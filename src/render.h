#ifndef GAME_RENDER
#define GAME_RENDER
#include "structs.h"

int draw_rectangle(struct rectangle*);
int draw_game(struct game_state*);
int draw_fps(struct game_state*, double);

#endif 
